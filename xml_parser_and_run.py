#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
from lxml.etree import iterparse
import os, csv
from pprint import pprint
# find input file(s)
letterfilenames = []
for root, onedir, files in os.walk(os.getcwd()):
    letterfilenames.extend([os.path.join(root, onefile) for onefile in files
                            if onefile.lower().startswith('letter') and onefile.lower().endswith('.xml')])
assert letterfilenames != [], 'no letter files found'
letterfilenames.sort()
# end find input file(s)

# load files
lettersarray = []
print('loading letters...'),
for infilename in letterfilenames:
    parsed = iterparse(infilename)
    for event, elem in parsed:
        if elem.tag.lower() in ('letter_gold', 'letter_grey', 'letter_planet', 'letter_red'):  # i.e. one record
            lettersarray.append({})
            lettersarray[-1]['Name'] = elem.findtext('_x0024_CLI-NOME-COGNO', default='')
            lettersarray[-1]['Address'] = elem.findtext('_x0024_CLI-IND', default='')
            lettersarray[-1]['City'] = elem.findtext('_x0024_CLI-IND3', default='')
            lettersarray[-1]['Title'] = elem.findtext('_x0024_CLI-TITLE', default='')
            lettersarray[-1]['Date'] = elem.findtext('_x0024_DATE', default='')
            lettersarray[-1]['Code'] = elem.findtext('_x0024_CODE', default='')
            lettersarray[-1]['Barcode'] = elem.findtext('_x0024_BARCODE', default='')
            elem.clear()
print(str(len(lettersarray)) + ' done')
pprint(lettersarray)

with open('export-csv.csv', 'w') as f:
    writer = csv.DictWriter(f, fieldnames= lettersarray[0].keys(),delimiter='\t')
    writer.writeheader()
    for each_record in lettersarray:
        writer.writerow(each_record)
print ('Exported records to csv ... done!')





